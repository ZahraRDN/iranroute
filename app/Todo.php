<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $table = 'todo';
    protected $fillable = ['title','description ','status','user_id'];
    protected $guarded=['id'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function tasks()
    {
        return $this->hasmany('App\Task');
    }

    public static function changeStatus($status,$id)
    {
        if(self::where('id',$id)->update(['status' => $status]))
            return true;
        else return false;
    }
}
