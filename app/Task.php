<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $table = 'tasks';
    public $fillable = ['title','description ','status','todo_id','due_date'];
    public $guarded=['id'];
    public $timestamps = false;

    public function todo()
    {
        return $this->belongsTo('App\Todo','todo_id');
    }

    public static function getAll($todo_id){
    	return self::where('todo_id',$todo_id)->orderBy('title','asc')->get();
    }

    public static  function allIs($status){
    	$status_count = self::where('status','!=',$status)->count();
    	if($status_count == 0)
    		return true;
    	return false;
    }

    public static function existIn($status){
    	$status_count = self::where('status',$status)->count();
    	if($status_count > 0)
    		return true;
    	return false;
    }
}
