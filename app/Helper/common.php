<?php

use App\Classes\Dialog;
use Illuminate\Support\HtmlString;
use App\Http\definition;
include_once '../app/Classes/status.php';


function show_dialogs()

{

	$Output        = null;

	$Message_Types = [

		'SUCCESS',

		'INFORMATION',

		'WARNING',

		'ERROR'

	];

	foreach ($Message_Types as $message_Type) {

		if (session()->has($message_Type)) {

			$Dialogs = session($message_Type);

			if (is_array($Dialogs)) {

				foreach ($Dialogs as $dialog) {

					$Output .= Dialog::$message_Type($dialog);

				}

			} else {

				$Output .= Dialog::$message_Type($Dialogs);

			}

			session()->flush($message_Type);

		}

	}

	return new HtmlString($Output);

}

if (!function_exists('add_dialog')) {

	function add_dialog($type, $data)

	{

		session()->put($type, $data);

	}

}

function check_remaining_date($date){
	$date_diff = date_diff(date_create(date('Y-m-d')),date_create($date));
	if($date_diff->format("%R") == '-')
		return 'EXPIRED';
	if($date_diff->format("%a") <= MIN_USUAL_DUE_DATE)
		return 'NOTICE';
	return 'USUAL';
}

function todo_status_should_be($todo_id){
	$class_statuses = get_statuses();
	$classes = get_status_classes($class_statuses);
	usort($classes, function($a, $b)
			{
			    return $a->priority > $b->priority;
			});
	
	foreach ($classes as $c) {
		if($c->confirm($todo_id))
			return true;
	}
	return false;

}

function get_statuses(){
	$statuses_class = get_php_classes(file_get_contents('../app/Classes/status.php'));
	return array_except($statuses_class,[array_search('Status', $statuses_class)]);
}

function get_php_classes($php_code) {
  $classes = array();
  $tokens = token_get_all($php_code);
  $count = count($tokens);
  for ($i = 2; $i < $count; $i++) {
    if (   $tokens[$i - 2][0] == T_CLASS
        && $tokens[$i - 1][0] == T_WHITESPACE
        && $tokens[$i][0] == T_STRING) {

        $class_name = $tokens[$i][1];
        $classes[] = $class_name;
    }
  }
  return $classes;
}

function get_status_classes($classes_name){

	$classes = [];$i = 0;$baseClass = 'Status';
	foreach ($classes_name as $s) {
		if (class_exists($s) && is_subclass_of($s, $baseClass)){
			$classes[$i] = new $s;
			if($classes[$i]->priority == '0'){
				unset($classes[$i]);
			} 
			else $i++;
		}
	}
	return $classes;

}

function get_status_classes_design(){
	$classes_design = [];
	$class_statuses = get_statuses();
	
	$classes = [];$i = 0;$baseClass = 'Status';
	foreach ($class_statuses as $s) 
		if (class_exists($s) && is_subclass_of($s, $baseClass)) $classes[$i++] = new $s;

	foreach ($classes as $c) 
		$classes_design[$c->get_name()] = $c->get_design_class();
	
	return $classes_design;
}