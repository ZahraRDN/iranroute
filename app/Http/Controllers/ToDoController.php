<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ToDoRequest;
use App\Todo;
use Auth;

class ToDoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $todo_list = Todo::where('user_id',Auth::id())->orderBy('title','asc')->get();
        $class_status = get_status_classes_design();
        return view('dashboard',compact('todo_list','class_status'));
    }

    public function store(ToDoRequest $r)
    {
    	$data = new Todo;
    	$data->title = $r->title;
    	$data->description = $r->description;
    	$data->user_id = Auth::id();
    	$data->status = "New";
    	if($data->save()) add_dialog('SUCCESS','Added to todo list successfully');
    	else add_dialog('ERROR','Try it again!');
    	return redirect('/dashboard');
    	
    }

    public function delete($id)
    {
    	$todo = Todo::where('id',$id)->first();
    	if(count($todo->tasks) == 0){
    		Todo::where('id',$id)->delete();
    		add_dialog('SUCCESS','Deleted from todo list successfully');
    	}
    	else add_dialog('ERROR','Selected todo has one or more tasks! You can`t delete it!');
    	return redirect('/dashboard');
    }

}
