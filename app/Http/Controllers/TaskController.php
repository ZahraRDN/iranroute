<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ToDoController;

use Illuminate\Http\Request;

use App\Http\Requests\TaskRequest;

use App\Task;

class TaskController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($todo_id)
    {
        $task = new Task;
       	$task_list = $task->getAll($todo_id);
    	$class_status = get_status_classes_design();
        $remaining = $this->get_remaining_due_date_statuses($task_list);
    	return view('tasks',compact('task_list','class_status','remaining','todo_id')) ;
    }

    public function delete($id)
    {
    	$task = Task::find($id);
    	if(count($task->tasks) == 0){
    		Task::where('id',$id)->delete();
    		add_dialog('SUCCESS','Deleted from task list successfully');
    	}
    	else add_dialog('ERROR','Selected task has one or more tasks! You can`t delete it!');
    	return redirect('/tasks/'.$task->todo_id);
    }

    public function store(TaskRequest $r)
    {
    	$data = new Task;
    	$data->title = $r->title;
    	$data->description = $r->description;
    	$data->todo_id = $r->todo_id;
    	$data->status = "New";
    	$data->due_date = $r->due_date;
    	if($data->save()) add_dialog('SUCCESS','Added to task list successfully');
    	else add_dialog('ERROR','Try it again!');
    	return redirect('/tasks/'.$r->todo_id);
    }

    public function changeTaskStatus(Request $r)
    {
    	$pr = Task::find($r->id);
    	if(Task::where('id',$r->id)->update(['status' => $r->status])){
    		if(todo_status_should_be($pr->todo_id)){
				add_dialog('SUCCESS','Change status was successfully.');
                return;
    		}
			Task::where('id',$r->id)->update(['status' => $pr->status]);
			add_dialog('ERROR','Change status failed!');
            return;
    	}
		add_dialog('ERROR','Change status failed!');
        return;
    }

    public function changeTaskDueDate(Request $r)
    {
        if(check_remaining_date($r->due_date) == 'EXPIRED'){
            add_dialog('ERROR','Change due date failed!EXPIRED DATE!');
            return;
        }
        $task = Task::find($r->id);
        if(Task::where('id',$r->id)->update(['due_date' => $r->due_date])){
            if($task->status == 'Failed')
                if(!Task::where('id',$r->id)->update(['status' => 'Extended']) && todo_status_should_be($task->todo_id))
                    add_dialog('ERROR','Change due date failed1!');
                return;

                add_dialog('SUCCESS','Change due date was successfully.');
                return;
                     
             }
        add_dialog('ERROR','Change due date failed!');
        return;
    }

    protected function get_remaining_due_date_statuses($tasks){
        $result = [];$i = 0;
        $class_due = ['NOTICE' => 'warning','EXPIRED' => 'danger', 'USUAL' => ''];
        foreach ($tasks as $t) {
            $result[$i++] = $class_due[check_remaining_date($t->due_date)];
        }
        return $result;
    }
}
