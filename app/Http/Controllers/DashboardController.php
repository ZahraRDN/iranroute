<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todo_list = Todo::where('user_id',Auth::id())->orderBy('title','asc')->get();
        $class_status = ['New' => 'success', 'Canceled' => 'danger', 'Done' => 'info'];
        return view('dashboard',compact('todo_list','class_status'));
    }
}
