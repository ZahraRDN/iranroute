<?php

namespace App\Http\Middleware;

use Closure;
use App\Task;
use Carbon\Carbon;

class CheckTaskDueDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$task_list)
    {
        $todo_id = $request->route()->parameters()['todo_id'];
        $tasks = Task::getAll($todo_id);
        foreach ($tasks as $task) {
            if(check_remaining_date($task->due_date) == 'EXPIRED' && $task->status != 'Failed'){
                Task::where('id',$task->id)->update(['status' => 'Failed']);
                todo_status_should_be($todo_id);
            }
        }
        return $next($request);
    }
}
