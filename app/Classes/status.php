<?php
	use App\Task;
	use App\Todo;

	abstract Class Status{
		protected $imported_status = 'Failed';
		protected $design_class;
		protected $priority;
		abstract public function confirm($todo_id);
		abstract public function get_design_class();
	}

	Class NewStatus extends Status{
		protected $name = 'New';
		public $priority = 4;
		protected $design_class = 'success';
		public function confirm($todo_id){
			if (Todo::changeStatus($this->name,$todo_id))
				return true;
			return false;
		}
		public function get_design_class(){
			return $this->design_class;
		}

		public function get_name(){
			return $this->name;
		}
	}

	Class DoneStatus extends Status{
		public $name = 'Done';
		public $priority = 2;
		protected $design_class = 'primary';
		public function confirm($todo_id){
			if(Task::existIn($this->name) && !Task::existIn($this->imported_status) && Todo::changeStatus($this->name,$todo_id)){
				return true;
			}
			return false;
		}
		public function get_design_class(){
			return $this->design_class;
		}

		public function get_name(){
			return $this->name;
		}
	}

	Class CanceledStatus extends Status{
		protected $name = 'Canceled';
		public $priority = 1;
		protected $design_class = '';
		public function confirm($todo_id){
			if(Task::allIs($this->name) && Todo::changeStatus($this->name,$todo_id))
				return true;
			return false;
		}
		public function get_design_class(){
			return $this->design_class;
		}

		public function get_name(){
			return $this->name;
		}
	}

	Class FailedStatus extends Status{
		protected $name = 'Failed';
		public $priority = 1;
		protected $design_class = 'danger';
		public function confirm($todo_id){
			if(Task::existIn($this->name) && Todo::changeStatus($this->name,$todo_id))
				return true;
			return false;
		}
		public function get_design_class(){
			return $this->design_class;
		}

		public function get_name(){
			return $this->name;
		}
	}

	Class ExtendedStatus extends Status{
		protected $design_class = 'info';
		protected $name = 'Extended';
		public $priority = 3;
		public function confirm($todo_id){
			if(Task::existIn($this->name) && Todo::changeStatus($this->name,$todo_id))
				return true;
			return false;
		}
		public function get_design_class(){
			return $this->design_class;
		}

		public function get_name(){
			return $this->name;
		}
	}
