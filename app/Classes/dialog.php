<?php

	namespace App\Classes;



	Class Dialog {

		var $Output;



		Static Function Information($Message)

		{

			return '

				<div class="co-md-12 alert alert-info">

				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

				  '.$Message.'

				</div>

			';

		}



		Static Function Error($Message)

		{

			return '

				<div class="co-md-12 alert alert-danger">

				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

				  '.$Message.'

				</div>

			';

		}



		Static Function Success($Message)

		{

			return '

				<div class="co-md-12 alert alert-success">

				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

				  '.$Message.'

				</div>

			';

		}


		Static Function Warning($Message)

		{

			return '

				<div class="co-md-12 alert alert-warning">

				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

				  '.$Message.'

				</div>

			';

		}

	}

?>