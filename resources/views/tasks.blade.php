@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{show_dialogs()}}
            <div class="panel panel-default">
                <div class="panel-heading" style=" font-weight: bold; " >Task list 
                    <span class='text-success' style=" float: right; cursor: pointer; " >
                        <i class="fa fa-plus-circle  fa-2x" aria-hidden="true" data-toggle="collapse" data-target="#add_task_list"></i>
                    </span>
                </div>
                <div class="panel-body">
                    <div id='add_task_list' class="collapse col-md-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-body">
                              <form role="form" method="post" action="{{ route('storeTask') }}">
                              {{ csrf_field() }}
                                <input type="hidden" name="todo_id" value="{{$todo_id}}">
                                <div class="form-group">
                                  <label for="title">Title:</label>
                                  <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                                  @if ($errors->has('title'))
                                    <span class="help-block" style=" color: #a94442 !important; " >
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                  <label for="description">Description:</label>
                                  <textarea class="form-control" id="description" placeholder="Enter description" name="description"></textarea>
                                  @if ($errors->has('description'))
                                    <span class="help-block" style=" color: #a94442 !important; ">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                  <label for="description">due time:</label>
                                  <input type="date" name="due_date" class="form-control" value='{{date("Y-m-d")}}'>
                                  @if ($errors->has('due_date'))
                                    <span class="help-block" style=" color: #a94442 !important; ">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                              </form>
                            </div>
                        </div>
                    </div>
                    <div style=" clear: both " ></div>
                    <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr class="active">
                            <th></th>
                            <th style=" text-align: center ">DEL</th>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th>DUE DATE</th>
                            <th style="  ">STATUS</th>
                          </tr>
                        </thead>
                        <tbody>
                        @php ($num = 0)
                        @if(count($task_list) == 0)
                          <h3 style=" text-align: center; " >No Task! You Should Insert Task First!</h3>
                        @else
                          @foreach($task_list as $task)
                            <tr>
                              <td>{{++$num}}</td>
                              <td style=" text-align: center "><a href='{{"/delTask/".$task->id}}' style="color:#a94442;" ><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
                              <td>{{$task->title}}</td>
                              <td>{{$task->description}}</td>
                              <td class="{{'label-'.$remaining[$num-1]}}"><input type="date" name="due_date" value="{{$task->due_date}}" id="due-date-{{$task->id}}" onchange="change_due_date({{$task->id}})"></td>
                              <td class="{{'label-'.$class_status[$task->status]}}" >
                                <select id="status-{{$task->id}}" style=" font-weight: bold;  " onchange="change_status({{$task->id}})" >
                                  @foreach(array_keys($class_status) as $status)
                                    <option class="{{'text-'.$class_status[$status]}}" @if($status == $task->status) {{'selected'}} @endif >{{$status}}</option>
                                  @endforeach
                                </select>
                              </td>
                            </tr> 
                          @endforeach  
                        @endif   
                        </tbody>
                      </table>
                      </div>
                      <div style=" clear: both " ></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
  function change_status(id) {
    var status = $('#status-'+id).val();
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

    $.ajax({
          type:"GET",
          url:"/changeTaskStatus",
          data:{ id:id,status:status},
      });
   location.reload();
  }

  function change_due_date(id) {
    var due_date = $('#due-date-'+id).val();
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

    $.ajax({
          type:"GET",
          url:"/changeTaskDueDate",
          data:{ id:id,due_date:due_date},
        // success: function(data) {
        //   alert(data);
        // }
      });
    location.reload();
  }
</script>
