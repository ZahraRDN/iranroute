@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{show_dialogs()}}
            <div class="panel panel-default">
                <div class="panel-heading" style=" font-weight: bold; " >To Do list 
                    <span class='text-success' style=" float: right; cursor: pointer; " >
                        <i class="fa fa-plus-circle  fa-2x" aria-hidden="true" data-toggle="collapse" data-target="#add_todo_list"></i>
                    </span>
                </div>
                <div class="panel-body">
                    <div id='add_todo_list' class="collapse col-md-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-body">
                              <form role="form" method="post" action="{{ route('storeToDo') }}">
                              {{ csrf_field() }}
                                <div class="form-group">
                                  <label for="title">Title:</label>
                                  <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                                  @if ($errors->has('title'))
                                    <span class="help-block" style=" color: #a94442 !important; " >
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                  <label for="description">Description:</label>
                                  <textarea class="form-control" id="description" placeholder="Enter description" name="description"></textarea>
                                  @if ($errors->has('description'))
                                    <span class="help-block" style=" color: #a94442 !important; ">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                              </form>
                            </div>
                        </div>
                    </div>
                    <div style=" clear: both " ></div>
                    <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr class="active">
                            <th></th>
                            <th style=" text-align: center ">DEL</th>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th style=" text-align: center ">STATUS</th>
                            <th style=" text-align: center ">MORE</th>
                          </tr>
                        </thead>
                        <tbody>
                        @php ($num = 0)
                        @if(count($todo_list) == 0)
                          <h3 style=" text-align: center; " >Nothing Exist! You Should Insert To Do First!</h3>
                        @else
                        @foreach($todo_list as $todo)
                          <tr>
                            <td>{{++$num}}</td>
                            <td style=" text-align: center "><a href='{{"delToDo/".$todo->id}}' style="color:#a94442;" ><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a></td>
                            <td>{{$todo->title}}</td>
                            <td>{{$todo->description}}</td>
                            <td class="{{'text-'.$class_status[$todo->status]}}" style=" text-align: center; font-weight: bold; " >{{$todo->status}}</td>
                            <td style=" text-align: center "><a href='{{"tasks/".$todo->id}}' style="color:#31708f;" ><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
                          </tr> 
                        @endforeach 
                        @endif    
                        </tbody>
                      </table>
                      </div>
                      <div style=" clear: both " ></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
