<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Auth Routes 
|--------------------------------------------------------------------------
|
*/
Auth::routes();

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegistrationController@confirm'
]);

/*
|--------------------------------------------------------------------------
| To do Routes 
|--------------------------------------------------------------------------
|
*/

Route::get('/dashboard', 'ToDoController@index')->name('dashboard');
Route::post('/storeToDo', 'ToDoController@store')->name('storeToDo');
Route::get('/delToDo/{todo_id}', 'ToDoController@delete')->name('delToDo');

/*
|--------------------------------------------------------------------------
| Task Routes 
|--------------------------------------------------------------------------
|
*/

Route::get('/tasks/{todo_id}', 'TaskController@index')->name('tasks')->middleware('DueDate:task_list');
Route::get('/delTask/{task_id}', 'TaskController@delete')->name('delTask');
Route::post('/storeTask', 'TaskController@store')->name('storeTask');
Route::get('/changeTaskStatus', 'TaskController@changeTaskStatus')->name('changeTaskStatus');
Route::get('/changeTaskDueDate', 'TaskController@changeTaskDueDate')->name('changeTaskDueDate');
